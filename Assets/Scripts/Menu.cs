using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game
{
    public class Menu : MonoBehaviour
    {
        [Header("Messages")]
        [SerializeField] GameObject _loseText;
        [SerializeField] GameObject _winText;
        [SerializeField] GameObject _congratsText;

        [Header("Buttons")]
        [SerializeField] GameObject _nextButton;
        [SerializeField] GameObject _exitButton;

        private bool _menuIsOpen = true;
        private int _currentSceneIndex;
        private int _bombSceneIndex = 2;
        
        private void OnEnable()
        {
            _currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
            _menuIsOpen = true;
        }

        private void Update()
        {
            if (!_menuIsOpen)
                return;
        }

        public void ShowWinMenu()
        {
            if (_currentSceneIndex == _bombSceneIndex)
            {
                _congratsText.SetActive(true);
                _exitButton.SetActive(true);
            }
            else
            {
                _winText.SetActive(true);
                _nextButton.SetActive(true);
            }
        }

        public void ShowLoseMenu()
        {
            _loseText.SetActive(true);
        }

        public void RestartLevel()
        {
            SceneManager.LoadScene(_currentSceneIndex);
        }

        public void NextLevel()
        {
            _currentSceneIndex++;
            SceneManager.LoadScene(_currentSceneIndex);
        }

        public void Exit()
        {
            EditorApplication.ExitPlaymode();
        }
    }
}