using System.Collections;
using UnityEngine;

namespace Game
{
    public class Bomb : MonoBehaviour
    {
        [SerializeField] private float _delay;
        [SerializeField] private ExplosionZone _explosionZonePrefab;

        private float _gridStep = 1.25f;

        private void Awake()
        {
            StartCoroutine(ExplosionDelay());
        }

        private IEnumerator ExplosionDelay()
        {
            yield return new WaitForSeconds(_delay);
            Explosion();
            Destroy(gameObject);
        }

        private void Explosion()
        {
            float yPosition = 0.2f;
            
            Instantiate(_explosionZonePrefab, new Vector3(transform.position.x, yPosition, transform.position.z), Quaternion.identity);
            Instantiate(_explosionZonePrefab, new Vector3(transform.position.x + _gridStep, yPosition, transform.position.z), Quaternion.identity);
            Instantiate(_explosionZonePrefab, new Vector3(transform.position.x - _gridStep, yPosition, transform.position.z), Quaternion.identity);
            Instantiate(_explosionZonePrefab, new Vector3(transform.position.x, yPosition, transform.position.z + _gridStep), Quaternion.identity);
            Instantiate(_explosionZonePrefab, new Vector3(transform.position.x, yPosition, transform.position.z - _gridStep), Quaternion.identity);
        }
    }
}
