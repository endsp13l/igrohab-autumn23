using UnityEngine;

namespace Game
{
    public class Portal : MonoBehaviour
    {
        [SerializeField] private Transform _portalExit;

        private void OnTriggerEnter(Collider other)
        {
            Vector2 exit = new Vector2(_portalExit.transform.position.x, _portalExit.position.z);
            Vector3 exitPosition = new Vector3(exit.x, other.transform.position.y, exit.y);

            other.gameObject.transform.position = exitPosition;
        }
    }
}