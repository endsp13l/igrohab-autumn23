using System.Collections;
using UnityEngine;

namespace Game
{
    public class ExplosionZone : MonoBehaviour
    {
        [SerializeField] private float _explosionLifetime;

        private void Awake()
        {
            StartCoroutine(ExplosionZoneCountdown());
        }

        private void OnCollisionEnter(Collision collision)
        {
            string collisionTag = collision.gameObject.tag;

            if (collisionTag == "Enemy" || collisionTag == "Destructable")
                Destroy(collision.gameObject);
            else if (collisionTag == "Player")
                collision.gameObject.GetComponent<Player>().Kill();
        }

        private IEnumerator ExplosionZoneCountdown()
        {
            yield return new WaitForSeconds(_explosionLifetime);
            Destroy(gameObject);
        }
    }
}