﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game
{
    [RequireComponent(typeof(Movement))]
    public class Player : MonoBehaviour
    {
        [SerializeField] private Bomb _bombPrefab;

        private Movement _movement;
        private Bomb _plantedBomb;
        private int _bombSceneIndex = 2;
      
        public bool IsAlive { get; private set; } = true;
        public bool HasKey { get; private set; } = false;

        private void Awake()
        {
            _movement = GetComponent<Movement>();
        }

        private void Start()
        {
            Enable();
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Space))
                TryPlantBomb();
        }

        public void Enable()
        {
            _movement.enabled = true;
        }

        public void Disable()
        {
            _movement.enabled = false;
        }

        public void Kill()
        {
            IsAlive = false;
        }

        public void PickUpKey()
        {
            HasKey = true;
        }

        private void TryPlantBomb()
        {
            if (_plantedBomb)
                return;

            if (SceneManager.GetActiveScene().buildIndex == _bombSceneIndex)
                _plantedBomb = Instantiate(_bombPrefab, transform.position, Quaternion.identity);
        }
    }
}